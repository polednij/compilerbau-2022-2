/*
 * Scanner fuer die Sprache c1
 */

%{
#include <stdlib.h>
#include "minako.h"
%}

%s COMMENT
%s STRING
%option noyywrap
%option noinput
%option nounput

DIGIT					[0-9]
INTEGER					{DIGIT}+
FLOAT					{INTEGER}"."{INTEGER}|"."{INTEGER}
CONST_INT				{INTEGER}
CONST_FLOAT				{FLOAT}([eE]([-+])?{INTEGER})?|{INTEGER}[eE]([-+])?{INTEGER}
CONST_BOOLEAN_TRUE		"true"
CONST_BOOLEAN_FALSE		"false"
LETTER					[a-zA-Z]
ID						({LETTER})+({DIGIT}|{LETTER})*

PLUS					"+"
MINUS					"-"
ASTERISK				"*"
SLASH					"/"
ASSIGN					"="

OPERATOR				{PLUS}|{MINUS}|{ASTERISK}|{SLASH}|{ASSIGN}

COMMA					","
SEMICOLON				";"
LPAREN					"("
RPAREN					")"
LBRACE					"{"
RBRACE					"}"

MISC					{COMMA}|{SEMICOLON}|{LPAREN}|{RPAREN}|{LBRACE}|{RBRACE}

%%


<INITIAL>{
"/*"					BEGIN(COMMENT);
"\""					BEGIN(STRING);
}

                        		/* C-Style Kommentare */
<COMMENT>{
"*/"                    BEGIN(INITIAL);
[^*\n]+
"*"
\n                      yylineno++;
}

                        		/* C++-Style Kommentare */
"//".*"\n"              yylineno++;

                                /* Strings */
<STRING>{
"\""                    BEGIN(INITIAL);
[^\n\"]*                { yylval.string = yytext; return (CONST_STRING); }
}

                                /* Leerzeilen */
\n                      yylineno++;


                                /* Whitespaces */
[ \t]+

                                /* Reservierte Woerter */
"bool"					{ return ( KW_BOOLEAN );        }
"do"					{ return ( KW_DO );             }
"else"					{ return ( KW_ELSE );           }
"float"					{ return ( KW_FLOAT );          }
"for"					{ return ( KW_FOR );            }
"if"					{ return ( KW_IF );             }
"int"					{ return ( KW_INT );            }
"printf"				{ return ( KW_PRINTF );         }
"return"				{ return ( KW_RETURN );         }
"void"					{ return ( KW_VOID );           }
"while"					{ return ( KW_WHILE );          }

                /* Bool'sche Operatoren */
"=="					{ return ( EQ );				}
"!="					{ return ( NEQ );				}
"<"						{ return ( LSS );				}
">"						{ return ( GRT );				}
"<="					{ return ( LEQ );				}
">="					{ return ( GEQ );				}
"&&"					{ return ( AND );				}
"||"					{ return ( OR );				}

                                /* Symbole */
{CONST_BOOLEAN_TRUE}	{ yylval.intValue = 1; 				return ( CONST_BOOLEAN );	}
{CONST_BOOLEAN_FALSE}	{ yylval.intValue = 0; 				return ( CONST_BOOLEAN );	}
{CONST_INT}				{ yylval.intValue = atoi(yytext); 	return ( CONST_INT );		}
{CONST_FLOAT}			{ yylval.floatValue = atof(yytext); return ( CONST_FLOAT );		}
{ID}					{ yylval.string = yytext; 			return ( ID );				}

                                /* andere Operatoren & Klammern */
{OPERATOR}|{MISC}		{ return ( yytext[0] );                 }

                                /* Dateiende */
<<EOF>>					{ return ( EOF );                       }

.						{ printf("Ungueltiges Zeichen: %s\nAbbruch!\n", yytext); return EOF;}
%%
