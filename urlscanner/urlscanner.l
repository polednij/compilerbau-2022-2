/*
 * Scanner fuer href Tags in xhtml Dateien
 */

%{
#include <stdlib.h>
#include "urlscanner.h"

char had_href = 0;
%}

%s ATAG
%s HREF
%s HREFSTRING
%s TEXT
%option noyywrap
%option noinput
%option nounput
%option nodefault

ATAG_CLOSE <\/a[ ]*>
ATAG_CLOSE_NL <\/a[ \n]*>

HREF_ATTRIBUT "href"


%%

<INITIAL>{
"<a"   BEGIN(ATAG);
}

<ATAG>{
{ATAG_CLOSE}        { BEGIN(INITIAL); had_href=0;               }
{ATAG_CLOSE_NL}     { BEGIN(INITIAL); yylineno++; had_href=0;   }
{HREF_ATTRIBUT}     { BEGIN(HREF); had_href=1;                  }
">"                 { if (had_href) BEGIN(TEXT);                }
[^<]
}

<HREF>{
"="
"\""                BEGIN(HREFSTRING);
}

<HREFSTRING>{
"\""                BEGIN(ATAG);
[^\n\"]*            { yylval = yytext; return TOKEN_URL; }
}

<TEXT>{
[^<]*               { BEGIN(ATAG); yylval=yytext; return TOKEN_TEXT; }
}

\n                  yylineno++;

<<EOF>>             return(EOF);

.

%%